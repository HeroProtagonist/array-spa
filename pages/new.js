import React from 'react'
import withRedux from 'next-redux-wrapper'
import Layout from '../components/shared/layout'
import CategoryNav from '../components/new/category-nav'
import ProductList from '../components/new/product-list'

import {
  initStore,
  getNavigation,
  getProductList,
  setSelectedList,
  toggleMobileSelect,
} from '../redux'

const New = props => {
  const {
    productList,
    selectedList,
    setSelectedList,
    activeListIndex,
    toggleMobileSelect,
    mobileSelect,
  } = props

  return (
    <Layout>
      <CategoryNav
        productList={productList}
        setSelectedList={setSelectedList}
        activeListIndex={activeListIndex}
        toggleMobileSelect={toggleMobileSelect}
        mobileSelect={mobileSelect}
      />
      <ProductList
        selectedList={selectedList}
      />
    </Layout>
  )
}

New.getInitialProps = async ({ store, query, req }) => {
  const { dispatch } = store
  // client side url is passed in, server side gender is used to get url
  let { url, gender, label = '', category } = query

  const isomorphicGender = gender || label.replace(' ', '') // match gender on server, label on client

  // only execute on server
  if (req) {
    const { navigation } = await dispatch(getNavigation())
    url = navigation.filter(nav => nav.label.toLowerCase().replace(' ', '') === gender)
    url = url[0].url
  }

  await dispatch(getProductList(url, isomorphicGender))
  const { productLists } = store.getState()
  const productList = productLists[isomorphicGender]


  // category comes from server, client it will default to 0
  let index = 0
  if (category) {
    index = productList.findIndex(product => product.header === category)
  }

  // set first list as default when on this page
  dispatch(setSelectedList(productList[index].products, index))

  return {
    productList,
  }
}

const mapStateToProps = state => ({
  selectedList: state.selectedList,
  activeListIndex: state.activeListIndex,
  mobileSelect: state.mobileSelect,
})

const mapDispatchToProps = dispatch => ({
  setSelectedList: (selectedList, ind) => dispatch(setSelectedList(selectedList, ind)),
  toggleMobileSelect: () => dispatch(toggleMobileSelect()),
})

export default withRedux(initStore, mapStateToProps, mapDispatchToProps)(New)

