import React from 'react'
import withRedux from 'next-redux-wrapper'
import Layout from '../components/shared/layout'
import Nav from '../components/index/nav'

import {
  initStore,
  getNavigation,
} from '../redux'

const Index = ({ navigation }) => (
  <Layout>
    <Nav navigation={navigation} />
  </Layout>
)

Index.getInitialProps = async ({ store, isServer }) => {
  const { dispatch } = store

  const { navigation } = await dispatch(getNavigation())

  return {
    navigation,
  }
}

export default withRedux(initStore)(Index)
