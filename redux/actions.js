import fetch from 'isomorphic-fetch'

import actionTypes from './action-types'

const {
  GET_NAVIGATION,
  GET_PRODUCT_LIST,
  SET_SELECTED_LIST,
  TOGGLE_MOBILE_SELECT,
} = actionTypes

export const getNavigation = () => async (dispatch, getState) => {
  let response
  try {
    response = await fetch(`${process.env.PROXY_URL}?path=/navigation`)
    response = await response.json()
  } catch (e) {
    response = [] // fix default
  }

  return dispatch({
    type: GET_NAVIGATION,
    navigation: response.nav[0].navGroups[0].navItems,
  })
}

export const getProductList = (genderUrl, isomorphicGender) => async (dispatch, getState) => {
  if (!genderUrl) return // stop back button from erroring

  let response
  try {
    response = await fetch(`${process.env.PROXY_URL}?path=/enhance-category${genderUrl}`)
    response = await response.json()
  } catch (e) {
    response = []
  }

  const { productList = [], pagination = {}, seoProperties = {} } = response

  return dispatch({
    type: GET_PRODUCT_LIST,
    gender: isomorphicGender,
    productList,
    pagination,
    seoProperties,
  })
}

export const setSelectedList = (selectedList, index) => ({
  type: SET_SELECTED_LIST,
  selectedList,
  index,
})

export const toggleMobileSelect = () => ({
  type: TOGGLE_MOBILE_SELECT,
})
