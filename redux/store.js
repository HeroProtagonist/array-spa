import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'

import rootReducer from './reducers'

const { NODE_ENV } = process.env

const initStore = initialState => (
  createStore(
    rootReducer,
    initialState,
    NODE_ENV !== 'production' ? composeWithDevTools(applyMiddleware(thunkMiddleware)) : applyMiddleware(thunkMiddleware),
  )
)

export default initStore
