import { combineReducers } from 'redux'

import actionTypes from './action-types'

const {
  GET_NAVIGATION,
  GET_PRODUCT_LIST,
  SET_SELECTED_LIST,
  TOGGLE_MOBILE_SELECT,
} = actionTypes

const navigation = (state = null, action) => {
  switch (action.type) {
    case GET_NAVIGATION:
      return action.navigation
    default:
      return state
  }
}

const productLists = (state = {}, action) => {
  switch (action.type) {
    case GET_PRODUCT_LIST:
      return { ...state, [action.gender]: action.productList }
    default:
      return state
  }
}

const selectedList = (state = [], action) => {
  switch (action.type) {
    case SET_SELECTED_LIST:
      return action.selectedList
    default:
      return state
  }
}

const activeListIndex = (state = 0, action) => {
  switch (action.type) {
    case SET_SELECTED_LIST:
      return action.index
    default:
      return state
  }
}

const mobileSelect = (state = false, action) => {
  switch (action.type) {
    case TOGGLE_MOBILE_SELECT:
      return !state
    default:
      return state
  }
}

const rootReducer = combineReducers({
  navigation,
  productLists,
  selectedList,
  activeListIndex,
  mobileSelect,
})

export default rootReducer
