import React from 'react'
import Link from 'next/link'

const Header = () => (
  <header
    className="header"
  >
    <Link
      href="/"
    >
      <a
        className="header__anchor"
      >
        <img
          className="header__image"
          src="https://www.jcrew.com//static/images/header-logo.1399d44217309fbe56afaa3e0ca380dd.svg"
          alt="jcrew logo"
        />
      </a>
    </Link>
  </header>
)


export default Header
