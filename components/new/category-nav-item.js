import React from 'react'
import Router from 'next/router'

const CategoriesNavItem = props => {
  const {
    header,
    products,
    setSelectedList,
    ind,
    activeListIndex,
    mobileSelect,
    toggleMobileSelect,
  } = props

  return (
    <div
      className={`
        category-nav__link
        ${(activeListIndex !== ind && !mobileSelect) && 'category-nav__link--hidden'}
        ${activeListIndex === ind && 'category-nav__link--active'}
      `}
      onClick={() => {
        const { label } = Router.query

        const as = {
          pathname: Router.pathname,
          query: {
            category: header,
            label,
          },
        }
        const href = {
          pathname: Router.asPath.split('?')[0],
          query: {
            category: header,
          },
        }

        Router.replace(as, href, { shallow: true })

        toggleMobileSelect()
        setSelectedList(products, ind)
      }}
    >
      {header}
    </div>
  )
}

export default CategoriesNavItem
