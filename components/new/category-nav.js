import React from 'react'
import CategoryNavItem from './category-nav-item'

const ClothingCategories = ({ productList, setSelectedList, activeListIndex, toggleMobileSelect, mobileSelect }) => {
  return (
    <nav className="category">
      <button
        className="category__button"
        onClick={toggleMobileSelect}
      >
        Change category
      </button>
      <ul className="category-nav">
        {productList.map(((category, ind) => {
          const { header, products } = category
          return (
            <li
              className="category-nav__items"
              key={ind}
            >
              <CategoryNavItem
                header={header}
                setSelectedList={setSelectedList}
                products={products}
                ind={ind}
                activeListIndex={activeListIndex}
                mobileSelect={mobileSelect}
                toggleMobileSelect={toggleMobileSelect}
              />
            </li>
          )
        }))}
      </ul>
    </nav>
  )
}

export default ClothingCategories
