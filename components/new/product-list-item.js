import React from 'react'

const ProductListItem = props => {
  const {
    productCode,
    defaultColorCode,
    productDescription,
  } = props

  return (
    <li
      className="product-list--item"
    >
      <figure
        className="product-list--figure"
      >
        <img
          className="product-list--img"
          src={`${process.env.JCREW_IMAGE_URL}/${productCode}_${defaultColorCode}`}
          alt={productDescription}
        />
        <figcaption>{productDescription}</figcaption>
      </figure>
    </li>
  )
}

export default ProductListItem
