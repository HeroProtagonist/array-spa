import React from 'react'
import ProductListItem from './product-list-item'

const ProductList = ({ selectedList }) => {
  return (
    <ul
      className="product-list"
    >
      {selectedList.map((item, ind) => <ProductListItem {...item} key={ind} />)}
    </ul>
  )
}

export default ProductList
