import React from 'react'
import NavItem from './nav-item'

const Nav = ({ navigation }) => (
  <nav>
    <ul className="nav">
      {navigation.map((item, ind) => {
        const { label, url } = item
        return (
          <li
            className="nav__items"
            key={ind}
          >
            <NavItem
              url={url}
              label={label}
            />
          </li>
        )
      })}
    </ul>
  </nav>
)

export default Nav
