import React from 'react'
import Link from 'next/link'

const NavItem = ({ label, url }) => {
  const labelLowerCase = label.toLowerCase()
  return (
    <Link
      href={`/new?label=${labelLowerCase}&url=${url}`}
      as={`/new/${labelLowerCase.replace(' ', '')}`}
    >
      <a
        className="nav__link"
      >
        {label}
      </a>
    </Link>
  )
}

export default NavItem
