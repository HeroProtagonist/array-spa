# SPA [Live Site](http://104.131.169.71/)

## Getting Started
clone repo

```
cp .env.sample .env
yarn install
yarn start
```
server will be running at `localhost:3000`


## Libraries
- Next.js - framework for isomorphic rendering of React. Supports code splitting by default.
  - Each page bundle is only the code needed for the specific route
- React/Redux (redux devtools)
- Express - server for next.js and used to proxy API
- node-sass/postcss - bundling, applying vendor prefixes, and minifying styles
- AirBnB styleguide - consistent code

## Future work
- Add CI and code coverage
- Add prop validation in React
- Check redux before doing network requests to prevent HTTP calls when data is already in the store
- Improve styling
- Add metadata for SEO
