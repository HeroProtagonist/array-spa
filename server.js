const express = require('express')
const next = require('next')
const fetch = require('isomorphic-fetch')

const {
  JCREW_BASE_URL,
  PORT,
  NODE_ENV,
} = process.env

const dev = NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare()
  .then(() => {
    const server = express()

    server.get('/api', (req, res) => {
      const { path } = req.query
      fetch(`${JCREW_BASE_URL}${path}`)
        .then(response => response.json())
        .then(response => res.send(response))
        .catch(() => res.send([]))
    })

    server.get('/new/:gender', (req, res) => {
      const { gender } = req.params
      const { category } = req.query

      return app.render(req, res, '/new', { gender, category })
    })

    server.get('*', (req, res) => handle(req, res))

    server.listen(PORT, (err) => {
      if (err) throw err
      console.log(`> Ready on http://localhost:${PORT}`)
    })
  })
